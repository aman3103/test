# magento2-install
Mac CLI command to install Magento 2.x.x + Docker + Redis + Xdebug + Sample Data for local Magento 2 development

This command allows you to easily spin up unlimited instances of any version Magento 2 for development on your local machine.  This is the exact same setup I have been using for years. It includes all of the goodies and extensions needed for M2 development. 


*Before installation please make sure you have downloaded Composer and Docker in your mac*


# Installation & Usage

1. Download the magento2-install file
2. Open the file in the text editor of your choice `open -a TextEdit /usr/local/bin/magento2-install`
4. Replace MAGENTO_PUBLIC_KEY and MAGENTO_PRIVATE_KEY with your Magento repository access keys. These will be needed to download Magento. You can get it from here (https://marketplace.magento.com/customer/accessKeys/)
5. Save your changes and close the file
6. Open a new terminal window
7. Issue command `sh magento2-install`
8. Answer any questions that are presented. The only question that requires an answer is the first one. You must specify which version of Magento you'd like to install. You can press enter to accept the defaults for the remaining questions.

Copied script form below link and modified:
https://www.magemodule.com/all-things-magento/magento2-freebies/magento2-docker-vm-script/


To stop the Docker vm, run the following command from the root of the Magento installation:

`docker-compose stop`

To tear down the Docker vm, run the following command from the root of the Magento installation:

`docker-compose down`

To spin the vm back up, run the following command from the root of the Magento installation:

`docker-compose up -d --build`

To run the Magento command connect to the web container 

`docker ps`

Get the container Id of web container and connect to bash:

`docker exec -it <Container ID> bash` 

`cd app`

`php bin/magento cache:clean`	

Or

`docker exec -it 919a6b93d28c /app/bin/magento cache:status`




